﻿/*
 * AX_Runtime.cs
 * 
 * Created By: Jason Penick
 * Email: StrikeBackStudios@gmail.com
 * 
 * Description:
 *
 * AX_Runtime is a set of Extension methods for the AXModel class. These extentions give developers
 * easier access to altering an AXModel's parameters at runtime.
 *
 *
 * License
 *
 * Copyright 2017 Jason Penick
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 *
 * This software (AX_Runtime.cs) is provided  free to use by anyone for use in their game/applicationw provided the following:
 *     1) This software (AX_Runtime) may not be included/distributed in any other "package", "asset", "tool", or "product"
 *        that has been, is currently, or will be sold on the Unity Asset Store. Use/Inclusion/Distribution in
 *        Compiled Games/Applications (commerical or otherwise) is completly acceptable, and the intended purpose of the software.
 *
 *     2) The header, copyright notice, and this permission notice shall be included in all copies of the software.
 */
using AX;


namespace SixThreeZero.AX
{

    public static class AX_RuntimeExtensions
    {

        public static AXParametricObject GetParametricObject(this AXModel model, string nodeName)
        {
            int count = model.parametricObjects.Count;
            for (int i = 0; i < count; i++)
            {
                if (model.parametricObjects[i].Name == nodeName)
                    return model.parametricObjects[i];
            }
            return null;
        }

        public static void SetParameterValue(this AXModel model, string nodeName, string paramName, bool value)
        {
            AXParametricObject target = model.GetParametricObject(nodeName);
            if (target == null)
                return;

            AXParameter axParam = target.getParameter(paramName);

            if (axParam == null)
                return;

            if (axParam.Type != AXParameter.DataType.Bool)
                return;

            axParam.intiateRipple_setBoolValueFromGUIChange(value);
        }

        public static void SetParameterValue(this AXModel model, string nodeName, string paramName, int value)
        {
            AXParametricObject target = model.GetParametricObject(nodeName);
            if (target == null)
                return;

            AXParameter axParam = target.getParameter(paramName);

            if (axParam == null)
                return;

            if (axParam.Type != AXParameter.DataType.Float)
                return;

            axParam.intiateRipple_setFloatValueFromGUIChange(value);
        }

        public static void SetParameterValue(this AXModel model, string nodeName, string paramName, float value)
        {
            AXParametricObject target = model.GetParametricObject(nodeName);
            if (target == null)
                return;

            AXParameter axParam = target.getParameter(paramName);

            if (axParam == null)
                return;

            if (axParam.Type != AXParameter.DataType.Float)
                return;

            axParam.intiateRipple_setFloatValueFromGUIChange(value);
        }

        public static int GetParameterIntValue(this AXModel model, string nodeName, string paramName)
        {
            AXParametricObject target = model.GetParametricObject(nodeName);
            if (target == null)
                throw new System.Exception("Model has no node named " + nodeName);


            AXParameter axParam = target.getParameter(paramName);

            if (axParam == null)
                throw new System.Exception("Model node has no parameter named " + paramName);


            if (axParam.Type != AXParameter.DataType.Int)
                throw new System.Exception("Model node parameter is not an integer");


            return axParam.IntVal;
        }

        public static float GetParameterFloatValue(this AXModel model, string nodeName, string paramName)
        {
            AXParametricObject target = model.GetParametricObject(nodeName);
            if (target == null)
                throw new System.Exception("Model has no node named " + nodeName);


            AXParameter axParam = target.getParameter(paramName);

            if (axParam == null)
                throw new System.Exception("Model node has no parameter named " + paramName);


            if (axParam.Type != AXParameter.DataType.Float)
                throw new System.Exception("Model node parameter is not an float");


            return axParam.FloatVal;
        }

        public static bool GetParameterBoolValue(this AXModel model, string nodeName, string paramName)
        {
            AXParametricObject target = model.GetParametricObject(nodeName);
            if (target == null)
                throw new System.Exception("Model has no node named " + nodeName);


            AXParameter axParam = target.getParameter(paramName);

            if (axParam == null)
                throw new System.Exception("Model node has no parameter named " + paramName);


            if (axParam.Type != AXParameter.DataType.Bool)
                throw new System.Exception("Model node parameter is not a Bool");


            return axParam.boolval;
        }

    }
}
