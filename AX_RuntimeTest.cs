﻿
using UnityEngine;
using AX;
using SixThreeZero.AX;

public class AX_RuntimeTest : MonoBehaviour {

    public AXModel myModel;
    public float recHeight;
    public float recWidth;
    public float extrudeHeight;


	// Use this for initialization
	void Start () {

        recWidth = myModel.GetParameterFloatValue("Rectangle", "width");
        recHeight = myModel.GetParameterFloatValue("Rectangle", "height");
        extrudeHeight = myModel.GetParameterFloatValue("Extrude_1", "Height");
    }
	
    void OnGUI()
    {

        bool shouldGenerate = false;

        GUILayout.BeginArea(new Rect(0, 0, 400, 400));
        GUILayout.BeginVertical();
        float newRecHeight = GUILayout.HorizontalSlider(recHeight, 1, 10);
        float newRecWidth = GUILayout.HorizontalSlider(recWidth, 1, 10);
        float newExtrudeHeight = GUILayout.HorizontalSlider(extrudeHeight, 1, 10);

        if (newRecHeight != recHeight)
        {
            recHeight = newRecHeight;
            myModel.SetParameterValue("Rectangle", "height", recHeight);
            shouldGenerate = true;
        }
        if (newRecWidth != recWidth)
        {
            recWidth = newRecWidth;
            myModel.SetParameterValue("Rectangle", "width", recWidth);
            shouldGenerate = true;
        }
        if (newExtrudeHeight != extrudeHeight)
        {
            extrudeHeight = newExtrudeHeight;
            myModel.SetParameterValue("Extrude_1", "Height", extrudeHeight);
            shouldGenerate = true;
        }
        if (shouldGenerate)
        {
            myModel.generate();
        }

        GUILayout.EndVertical();
        GUILayout.EndArea();
    }
}